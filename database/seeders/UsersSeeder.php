<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super = Role::create(['name' => 'Superintendent']);
        $principal = Role::create(['name' => 'Principal']);
        $teacher = Role::create(['name' => 'Teacher']);
        $student = Role::create(['name' => 'Student']);

        $sh_year = Permission::create(['name' => 'see school year']);
        $cr_year = Permission::create(['name' => 'create school year']);
        $ed_year = Permission::create(['name' => 'edit school year']);
        $rm_year = Permission::create(['name' => 'remove school year']);

        $sh_group = Permission::create(['name' => 'see group']);
        $cr_group = Permission::create(['name' => 'create group']);
        $ed_group = Permission::create(['name' => 'edit group']);
        $rm_group = Permission::create(['name' => 'remove group']);

        $sh_class = Permission::create(['name' => 'see class']);
        $cr_class = Permission::create(['name' => 'create class']);
        $ed_class = Permission::create(['name' => 'edit class']);
        $rm_class = Permission::create(['name' => 'remove class']);

        $sh_grade = Permission::create(['name' => 'see grades']);
        $gd_grade = Permission::create(['name' => 'grade assignment']);
        $ed_grade = Permission::create(['name' => 'edit grade']);
        $rm_grade = Permission::create(['name' => 'remove grade']);

        $sh_assignment = Permission::create(['name' => 'see assignments']);
        $cr_assignment = Permission::create(['name' => 'create assignment']);
        $ed_assignment = Permission::create(['name' => 'edit assignment']);
        $rm_assignment = Permission::create(['name' => 'remove assignment']);


//        $principal->givePermissionTo(
//            $sh_year, $cr_year, $ed_year, $rm_year,
//            $sh_group, $cr_group, $ed_group, $rm_group,
//            $sh_class, $cr_class, $ed_class, $rm_class,
//            $sh_grade, $gd_grade, $ed_grade, $rm_grade,
//            $sh_assignment, $cr_assignment, $ed_assignment, $rm_assignment,
//        );
//
//        $teacher->givePermissionTo(
//            $sh_group, $cr_group, $ed_group, $rm_group
//        );

        $us_super = User::create([
            "name" => "super",
            "email" => "super@g.com",
            "password" => Hash::make('super')
        ]);

        $us_super->assignRole($super);

        $us_principal = User::create([
            "name" => "principal",
            "email" => "principal@g.com",
            "password" => Hash::make('principal')
        ]);

        $us_principal->assignRole($principal);

        $us_teacher = User::create([
            "name" => "teacher",
            "email" => "teacher@g.com",
            "password" => Hash::make('teacher')
        ]);

        $us_teacher->assignRole($teacher);

        $us_student = User::create([
            "name" => "student",
            "email" => "student@g.com",
            "password" => Hash::make('student')
        ]);

        $us_student->assignRole($student);
    }
}
