<?php

namespace Database\Seeders;

use App\Models\SchoolYear;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class YearsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolYear::create([
           'name' => '2019',
           'start_date' => Carbon::parse('2019-01-01'),
           'end_date' => Carbon::parse('2019-12-31'),
        ]);

        SchoolYear::create([
            'name' => '2020',
            'start_date' => Carbon::parse('2020-01-01'),
            'end_date' => Carbon::parse('2020-12-31'),
        ]);

        SchoolYear::create([
            'name' => '2021',
            'start_date' => Carbon::parse('2021-01-01'),
            'end_date' => Carbon::parse('2021-12-31'),
        ]);
    }
}
