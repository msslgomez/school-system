<?php

namespace Database\Seeders;

use App\Models\Career;
use App\Models\SchoolYear;
use Illuminate\Database\Seeder;

class CareersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Career::create([
            "name" => "Engineering",
            "code" => "0789"
        ]);

        Career::create([
            "name" => "Teaching",
            "code" => "7890"
        ]);

        Career::create([
            "name" => "Nursing",
            "code" => "8901"
        ]);

        Career::create([
            "name" => "Doctor",
            "code" => "9010"
        ]);

        Career::create([
            "name" => "Accoutant",
            "code" => "0123"
        ]);

        Career::create([
            "name" => "Business",
            "code" => "1234"
        ]);
    }
}
