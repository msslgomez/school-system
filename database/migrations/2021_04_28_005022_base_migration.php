<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BaseMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->timestamps();
        });

        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('address');
            $table->timestamps();
        });

        Schema::create('school_years', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('start_date')->unique();
            $table->date('end_date')->unique();
            $table->timestamps();
        });

        Schema::create('school_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->bigInteger('career_id')->unsigned();
            $table->bigInteger('school_year_id')->unsigned();
            $table->bigInteger('prerequisite_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('career_id')->references('id')->on('careers');
            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->foreign('prerequisite_id')->references('id')->on('school_classes');
        });

        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('class_id')->unsigned();
            $table->timestamps();
            $table->foreign('class_id')->references('id')->on('school_classes');
        });

        Schema::create('group_user', function (Blueprint $table) {
            $table->bigInteger('group_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('instructions')->nullable();
            $table->string('resources')->nullable();
            $table->dateTime('schedule')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('points')->nullable();
            $table->bigInteger('class_id')->unsigned();
            $table->timestamps();
            $table->foreign('class_id')->references('id')->on('school_classes');
        });

        Schema::create('assignment_turn_ins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('turn_in_date');
            $table->bigInteger('assignment_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();
            $table->timestamps();
            $table->foreign('assignment_id')->references('id')->on('assignments');
            $table->foreign('student_id')->references('id')->on('users');
        });

        Schema::create('grades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('class_id')->unsigned();
            $table->bigInteger('assignment_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();
            $table->float('grade');
            $table->timestamps();
            $table->foreign('class_id')->references('id')->on('school_classes');
            $table->foreign('assignment_id')->references('id')->on('assignments');
            $table->foreign('student_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers');
        Schema::dropIfExists('school_classes');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_user');
        Schema::dropIfExists('assignments');
        Schema::dropIfExists('assignment_turn_ins');
        Schema::dropIfExists('grades');
    }
}
