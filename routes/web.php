<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm']);

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('/users', App\Http\Controllers\UserController::class);
    Route::resource('/classes', App\Http\Controllers\ClassController::class);
    Route::resource('/careers', App\Http\Controllers\CareerController::class)->except(['create', 'edit']);
    Route::get('/careers/data', [\App\Http\Controllers\CareerController::class, 'getData']);


    Route::resource('/years', App\Http\Controllers\SchoolYearController::class);
    Route::resource('/schools', App\Http\Controllers\SchoolController::class);
    Route::resource('/assignments', App\Http\Controllers\AssignmentController::class);




});

