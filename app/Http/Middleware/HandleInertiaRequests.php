<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Inertia\Middleware;
use Spatie\Permission\Models\Role;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param \Illuminate\Http\Request $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function share(Request $request)
    {
        $auth = null;
        $perms = null;

        if (Auth::user()) {
            $roles = Auth::user()->roles()->pluck('name')->toArray();

            if (in_array('Superintendent', $roles)) {
                $perms = [
                    [
                        'url' => '/',
                        'icon' => 'fa fa-home',
                        'name' => 'Home',
                    ],
                    [
                        'url' => '/years',
                        'icon' => 'fa fa-home',
                        'name' => 'School Years',
                    ],
                    [
                        'url' => '/schools',
                        'icon' => 'fa fa-home',
                        'name' => 'Schools',
                    ],
                    [
                        'url' => '/careers',
                        'icon' => 'fa fa-home',
                        'name' => 'Careers',
                    ],
                    [
                        'url' => '/classes',
                        'icon' => 'fa fa-home',
                        'name' => 'Classes',
                    ],
                    [
                        'url' => '/users',
                        'icon' => 'fa fa-home',
                        'name' => 'Users',
                    ],
                ];
            } else if (in_array('Principal', $roles)) {
                $perms = [
                    [
                        'url' => '/',
                        'icon' => 'fa fa-home',
                        'name' => 'Home',
                    ],
                    [
                        'url' => '/careers',
                        'icon' => 'fa fa-home',
                        'name' => 'Careers',
                    ],
                    [
                        'url' => '/classes',
                        'icon' => 'fa fa-home',
                        'name' => 'Classes',
                    ],
                    [
                        'url' => '/users',
                        'icon' => 'fa fa-home',
                        'name' => 'Users',
                    ],
                ];
            } else if (in_array('Teacher', $roles)) {
                $perms = [
                    [
                        'url' => '/',
                        'icon' => 'fa fa-home',
                        'name' => 'Home',
                    ],
                    [
                        'url' => '/classes',
                        'icon' => 'fa fa-home',
                        'name' => 'Classes',
                    ],
                    [
                        'url' => '/assignments',
                        'icon' => 'fa fa-home',
                        'name' => 'Assignments',
                    ],
                ];
            } else {
                $perms = [

                ];
            }

            $auth = [
                'id' => Auth::user()->id,
                'name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'role' => Auth::user()->roles()->pluck('name'),
                'perms' => $perms
            ];
        }


        return array_merge(parent::share($request), [

            'app' => [
                'name' => 'School System',
            ],
            'auth' => $auth,
            'success' => Session::get('success'),
            'info' => Session::get('info'),
            'data' => Session::get('data'),
            'errors' => Session::get('errors') ? Session::get('errors')->getBag('default')->getMessages() : (object)[],
        ]);
    }
}
