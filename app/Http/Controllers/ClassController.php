<?php

namespace App\Http\Controllers;

use App\Models\Career;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Classes/Index', [
            'classes' => SchoolClass::with('career', 'prerequisite', 'year')->orderByDesc('updated_at')->get(),
            'years' => SchoolYear::orderByDesc('updated_at')->get(),
            'careers' => Career::orderByDesc('updated_at')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required|alpha_num|unique:school_classes',
            'career_id' => 'required',
            'school_year_id' => 'required',
        ]);

        DB::beginTransaction();
        $class = new SchoolClass();
        $class->fill($validatedData);
        $class->save();
        DB::commit();

        return redirect('/classes');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required|alpha_num|unique:school_classes,code,' . $id,
            'career_id' => 'required',
            'school_year_id' => 'required',
            'prerequisite_id' => 'nullable'
        ]);

        DB::beginTransaction();
        $class = SchoolClass::findOrFail($id);
        $class->fill($validatedData);
        $class->save();
        DB::commit();

        return redirect('/classes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $class = SchoolClass::findOrFail($id);
        $class->delete();
        DB::commit();

        return redirect('/classes');
    }
}
