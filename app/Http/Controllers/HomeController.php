<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $roles = Auth::user()->roles()->pluck('name')->toArray();

        if (in_array('Superintendent', $roles)) {
            return Inertia::render('Home/SuperIndex');
        } else if (in_array('Principal', $roles)) {
            return Inertia::render('Home/PrincipalIndex');
        } else if (in_array('Teacher', $roles)) {
            return Inertia::render('Home/TeacherIndex');
        } else {
            return Inertia::render('Home/StudentIndex');
        }

    }
}
