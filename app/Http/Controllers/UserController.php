<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $roles = Role::orderByDesc('updated_at')->get();

        $roles->map(function ($role) {
            $role['value'] = $role['id'];
            $role['text'] = $role['name'];
        });

        return Inertia::render('Users/Index', [
            'users' => User::with('roles')->orderByDesc('updated_at')->get(),
            'roles' => $roles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'role' => 'required'
        ]);

        DB::beginTransaction();
        $user = new User();
        $user->fill($validatedData);
        $user->fill([
            'password' => Hash::make($request->password)
        ]);
        $user->save();
        DB::commit();

        $role = Role::findById($request->role);
        $user->assignRole($role);

        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|max:255|unique:users,email,' . $id,
            'role' => 'required',
        ]);

        DB::beginTransaction();
        $user = User::findOrFail($id);
        $user->fill($validatedData);

        if ($request->has('password_edit')) {
            $user->fill([
                'password' => Hash::make($request->password_edit)
            ]);
        }
        $user->save();
        DB::commit();

        $role = Role::findById($request->role);
        $user->syncRoles($role);

        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $user = User::findOrFail($id);
        $user->delete();
        DB::commit();

        return redirect('/users');
    }
}
