<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\SchoolClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        return Inertia::render('Assignments/Index', [
            'assignments' => Assignment::with('class')->orderByDesc('updated_at')->get(),
            'classes' => SchoolClass::orderByDesc('updated_at')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'instructions' => 'required|max:255',
            'resource' => 'nullable|mimes:pdf',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'points' => 'required|numeric',
            'class_id' => 'required|exists:school_classes,id'
        ]);

        DB::beginTransaction();
        $assignment = new Assignment();
        $assignment->fill($validatedData);

        if ($request->resource) {
            $path = Storage::putFile('assignments', $request->resource);
            $assignment->resources = $path;
        }

        $assignment->save();
        DB::commit();

        return redirect('/assignments');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());

        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'instructions' => 'required|max:255',
            'resources' => 'nullable|mimes:pdf',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'points' => 'required|numeric',
            'class_id' => 'required|exists:school_classes,id'
        ]);

        DB::beginTransaction();
        $assignment = Assignment::findOrFail($id);
        $assignment->fill($validatedData);
        $assignment->save();
        DB::commit();

        return redirect('/assignments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $assignment = Assignment::findOrFail($id);
        $assignment->delete();
        DB::commit();

        return redirect('/assignments');
    }
}
