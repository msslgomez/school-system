<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignmentTurnIn extends Model
{
    use HasFactory;

    protected $table = 'assignment_turn_ins';

    public function assignment()
    {
        $this->belongsTo(Assignment::class);
    }

    public function student()
    {
        $this->belongsTo(User::class);
    }
}
