<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'start_date', 'end_date'];
    protected $appends = ['value', 'text'];

    public function getValueAttribute() {
        return $this->id;
    }

    public function getTextAttribute() {
        return $this->name;
    }

    public function career()
    {
        return $this->belongsToMany(Career::class);
    }
}
