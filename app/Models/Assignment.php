<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'instructions', 'resources', 'start_date', 'end_date', 'points', 'class_id'];

    public function class()
    {
        return $this->belongsTo(SchoolClass::class);
    }
}
