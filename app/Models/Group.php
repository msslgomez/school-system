<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = ['name, class_id'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'group_user');
    }

    public function class()
    {
        return $this->belongsTo(SchoolClass::class);
    }
}
