<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'code'];
    protected $appends = ['value', 'text'];

    public function getValueAttribute() {
        return $this->id;
    }

    public function getTextAttribute() {
        return $this->name;
    }

    public function classes()
    {
        return $this->hasMany(SchoolClass::class);
    }
}
