<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'code', 'career_id', 'school_year_id', 'prerequisite_id'];
    protected $appends = ['value', 'text'];

    public function getValueAttribute()
    {
        return $this->id;
    }

    public function getTextAttribute()
    {
        return $this->name;
    }

    public function year()
    {
        return $this->belongsTo(SchoolYear::class);
    }

    public function career()
    {
        return $this->belongsTo(Career::class);
    }

    public function prerequisite()
    {
        return $this->belongsTo(SchoolClass::class);
    }

    public function grades()
    {
        return $this->hasMany(Grade::class);
    }

    public function assignments()
    {
        return $this->hasMany(Assignment::class);
    }
}
